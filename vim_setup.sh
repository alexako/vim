#!/bin/bash

# Prerequisite:
# Ubuntu
sudo apt-get build-dep vim

# Vim Set up
cd vim/src
./configure --enable-pythoninterp --with-features=huge/ vim
make && make install
mkdir -p $HOME/bin
cd $HOME/bin
ln -s $HOME/opt/vim/bin/vim
which vim
vim --version

# on Ubuntu
sudo apt-get install gtk2-engine-pixbuf
