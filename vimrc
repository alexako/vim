" default settings
syntax on
set ruler
set number
set tabstop=2
set shiftwidth=2
set expandtab
set autoindent
set list listchars=tab:▷⋅,trail:⋅,nbsp:⋅

" vim-plug setup
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" vim-plug plugins
call plug#begin('~/.vim/plugged')
Plug 'junegunn/vim-easy-align'
" Any valid git URL is allowed
Plug 'https://github.com/junegunn/vim-github-dashboard.git'
"Group dependencies, vim-snippets depends on ultisnips
Plug 'SirVer/ultisnips' | Plug 'honza/vim-snippets'
"bold On-demand loading
Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }
Plug 'scrooloose/nerdcommenter'
" Syntastic
Plug 'scrooloose/syntastic'
" Using a non-master branch
"Plug 'rdnetto/YCM-Generator', { 'branch': 'stable' }
" Using a tagged release; wildcard allowed
Plug 'fatih/vim-go', { 'tag': '*' }
" Plugin options
Plug 'nsf/gocode', { 'tag': 'v.20150303', 'rtp': 'vim' }
" Plugin outside ~/.vim/plugged with post-update hook
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': '.install --all' }
" Unmanaged plugin (manually installed and updated)
Plug '~/my-prototype-plugin'
" Typescript Syntax hightlighting
Plug 'leafgarland/typescript-vim'
" youcompleteme
"Plug 'valloric/youcompleteme'
" matchit
Plug 'vim-scripts/matchit.zip'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-fugitive'
Plug 'chriskempson/tomorrow-theme'
Plug 'jeffkreeftmeijer/vim-numbertoggle'
Plug 'pangloss/vim-javascript'
Plug 'ervandew/supertab'
" Add plugins to &runtimepath
Plug 'mhartington/oceanic-next'
call plug#end()
" END vim-plug

" Syntastic Recommended settings
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

syntax enable
"colorscheme Tomorrow-Night-Bright
colorscheme OceanicNext

" Keep background transparent
"hi NonText ctermbg=NONE
hi Normal guibg=NONE ctermbg=NONE
"hi LineNr ctermfg=103 ctermbg=None guibg=NONE

" NerdTree
silent! nmap <F2> :NERDTreeToggle<CR>
"silent! map <F2> :NERDTreeFind<CR>
autocmd VimEnter * NERDTree
"let g:NERDTreeMapActivateNode="<F2>"
let g:NERDTreeMapPreview="<F4>"

" make backspace more powerful
set backspace=indent,eol,start

" HTML/CSS settings
au FileType .html set tabstop=2
au FileType css set tabstop=2
au BufRead,BufNewFile *.html.erb set tabstop=2
au BufRead,BufNewFile *.js set tabstop=2
au BufRead,BufNewFile *.html.erb set shiftwidth=2
au BufRead,BufNewFile *.js set shiftwidth=2

" Python settings
set tabstop=4
set shiftwidth=4
set statusline=%F%m%r%h%w\ [TYPE=%Y\ %{&ff}]\
\ [%l/%L\ (%p%%)
filetype plugin indent on
au FileType py set autoindent
au FileType py set smartindent
au FileType py set textwidth=79 " PEP-8 Friendly
